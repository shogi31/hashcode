_ = require 'lodash'

###
Principe trouver un carré avec les numéros de 1 à 16 dont il est possible de trouver 34 en sommant les collones, les lignes, les diagonales, etc...
Index:
 0  1  2  3
 4  5  6  7
 8  9 10 11
12 13 14 15
###

N = 16

print = ->
  console.log '--'
  console.log _.map([[0..3], [4..7], [8..11], [12..15]], (r)-> _.map(r, (i)->combi[i]).join("\t")).join("\n")

tests = for n in [0..N-1] then switch n
  when 3 then [[0,1,2,3]]
  when 5 then [[0,1,4,5]]
  when 6 then [[1,2,5,6]]
  when 7 then [[4,5,6,7], [2,3,6,7]]
  when 9 then [[4,5,8,9]]
  when 10 then [[5,6,9,10], [0,2,8,10]]
  when 11 then [[8,9,10,11], [6,7,10,11], [1,3,9,11], [4,7,8,11]]
  when 12 then [[0,4,8,12], [3,6,9,12]]
  when 13 then [[1,5,9,13], [8,9,12,13], [2,7,8,13]]
  when 14 then [[2,6,10,14], [4,6,12,14], [9,10,13,14], [1,4,11,14], [1,2,13,14]]
  when 15 then [[12,13,14,15], [3,7,11,15], [10,11,14,15], [0,3,12,15], [5,7,13,15], [0,5,10,15]]
  else []

check = (n)->
  for test in tests[n] then if _.sum(_.map(test, (i)-> combi[i])) != 34 then return false
  return true

fillCase = (n)->
  # test all number
  for i in [0..N - 1]
    # skip if already used
    if used[i] then continue
    combi[n] = i + 1
    used[i] = true
    # test partial compatibility
    if check n
      # fill the next case
      if n < N - 1 then fillCase n + 1
      # display result
      else
        print()
        sum++
    # free the number and test the next
    used[i] = false

console.log '--------'
t = new Date()
sum = 0
combi = for i in [0..N-1] then null
used = for i in [0..N-1] then false

# fill the first case
fillCase 0

console.log '--------'
console.log "#{N}\t#{(new Date() - t)}\t#{sum}"
