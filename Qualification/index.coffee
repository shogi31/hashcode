#score = 0
#powww_ = null
#
#for powww in [300..340]

console.log '----------'
fs = require 'fs'
_ = require 'lodash'

data = fs.readFileSync('./dc.in').toString().split('\n')

[R, S, U, P, M] = data[0].split(' ').map (t)-> +t

console.log 'R, S, U, P, M'
console.log R, S, U, P, M

#
# Génération de la grille
#

grid =
  for r in [0..R - 1]
    for s in [0..S - 1]
      '.'

for i in [1..U]
  [r, s] = data[i].split(' ').map (t)-> +t
  grid[r][s] = ' '

print =
  for r in [0..R - 1]
    grid[r].join('')
fs.writeFileSync('grid', print.join('\n'))

#
# Génération de la liste des serveurs
#

server = for i in [0 .. M - 1]
  [z, c] = data[U + 1 + i].split(' ').map (t)-> +t
  {i: i, z: z, c: c}

#
# Rangement des serveurs par efficacité
#

byCapacity = _.clone server
byCapacity.sort (a, b)-> -a.c + b.c
byEfficiency = _.clone server
pow = 2.05
byEfficiency.sort (a, b)-> (-(a.c**pow / a.z) + (b.c**pow / b.z)) * 100 - a.z + b.z

#
# Génération des lignes et des ses sections
#

lines =
  for r in [0..R - 1]
    s = 0
    section = grid[r].join('').split(' ').map (t, i)->
      s_ = s
      s = s + 1 + t.length
      {i: i, s: s_, size: t.length, available: t.length}
    {r: r, section: section, capacity: 0, server: []}

#
# Répartition des serveurs sur les rangées
#

getLine = (lines, z)->
  for line in lines
    for sec in line.section
      if sec.available >= z
        return line

for ser in byEfficiency
  lines.sort (a, b)-> a.capacity - b.capacity
  line = getLine lines, ser.z
  if !line then continue
  line.section.sort (a, b)-> -a.available + b.available
  section = line.section[0]
  line.server.push ser
  section.available -= ser.z
  line.capacity += ser.c
  ser.line = line
  ser.s = section.s + section.available
  for s in [section.s + section.available .. section.s + section.available + ser.z - 1]
    grid[line.r][s] = ser.z


#
# Minimisation des écarts entre les rangés beau mais inéficace
#

switchLine = (littleLine, bigLine)->
  delta = bigLine.capacity - littleLine.capacity
  diffs = []
  for size in [5..1]
    littleSer = _.filter littleLine.server, (ser) -> ser.z == size
    bigSer = _.filter bigLine.server, (ser) -> ser.z == size
    for lit in littleSer
      for big in bigSer
        diffs.push {big: big, lit: lit, newDelta: Math.abs(delta + (lit.c - big.c) * 2)}
  diffs.sort (a, b)-> a.newDelta - b.newDelta
  if diffs[0].newDelta >= delta then return false
  diff = diffs[0]
  litIndex = littleLine.server.indexOf diff.lit
  bigIndex = littleLine.server.indexOf diff.big
  littleLine.server[litIndex] = diff.big
  bigLine.server[bigIndex] = diff.lit
  littleLine.capacity += diff.big.c - diff.lit.c
  bigLine.capacity += diff.lit.c - diff.big.c
  s_ = diff.big.s
  diff.big.line = littleLine
  diff.big.r = littleLine.r
  diff.big.s = diff.lit.s
  diff.lit.line = bigLine
  diff.lit.r = bigLine.r
  diff.lit.s = s_
  true

#diff = true
while diff
  lines.sort (a, b)-> a.capacity - b.capacity
  diff = switchLine lines[0], lines[R - 1]

#
# Génération des groupes
#

groups =
  for p in [0..P - 1]
    {p: p, gc: 0, server: [], capacity: [], c: 1000}

    #
    # On mets les gros serveurs ensembles
    #

getGroup0 = (groups, r, c)->
  for group in groups
    if group.gc + c <= 304
      return group

for ser in byCapacity
  unless ser.line then continue
  group = getGroup0 groups, ser.line.r, ser.c
  unless group then continue
  ser.group = group
  line = ser.line.r
  group.server.push ser
  group.capacity[line] ?= 0
  group.capacity[line] += ser.c
  sum = 0
  for c in group.capacity then if c then sum += c
  group.gc = _.sum(group.capacity) - _.max(group.capacity)
  group.c = _.sum(group.capacity)
  group.gcl = group.capacity.indexOf _.max(group.capacity)

#
# Répartition des serveurs restants
#

getGroup = (groups, r)->
  return groups[0]
  for group in groups
    if group.gc != group.capacity[r]
      return group
  return groups[0]

for ser in byEfficiency
  unless ser.line then continue
  if ser.group then continue
  groups.sort (a, b)-> a.gc * (1 + (a.capacity[ser.line.r] ? 0) / 500) - b.gc * (1 + (b.capacity[ser.line.r] ? 0) / 500)
  group = getGroup groups, ser.line.r
  ser.group = group
  line = ser.line.r
  group.server.push ser
  group.capacity[line] ?= 0
  group.capacity[line] += ser.c
  sum = 0
  for c in group.capacity then if c then sum += c
  group.gc = _.sum(group.capacity) - _.max(group.capacity)
  group.c = _.sum(group.capacity)
  group.gcl = group.capacity.indexOf _.max(group.capacity)

#
# Maximisation du min
#

switchGroup = (littleGroup)->
  diffs = []
  for bigGroup in groups
    for lit in littleGroup.server then if lit.c <= 100
      for big in bigGroup.server then if big.c <= 100
        litCapacity = _.clone littleGroup.capacity
        bigCapacity = _.clone bigGroup.capacity
        litCapacity[lit.line.r] -= lit.c
        litCapacity[big.line.r] += big.c
        bigCapacity[lit.line.r] += lit.c
        bigCapacity[big.line.r] -= big.c
        litGc = _.sum(litCapacity) - _.max(litCapacity)
        bigGc = _.sum(bigCapacity) - _.max(bigCapacity)
        diffs.push {bigGroup: bigGroup, big: big, lit: lit, newGc: Math.min(bigGc, litGc)}
  diffs.sort (a, b)-> -a.newGc + b.newGc
  if diffs[0].newGc <= littleGroup.gc then return false
  diff = diffs[0]
  bigGroup = diff.bigGroup
  lit = diff.lit
  big = diff.big
  littleGroup.capacity[lit.line.r] -= lit.c
  littleGroup.capacity[big.line.r] += big.c
  bigGroup.capacity[lit.line.r] += lit.c
  bigGroup.capacity[big.line.r] -= big.c
  litIndex = littleGroup.server.indexOf diff.lit
  bigIndex = bigGroup.server.indexOf diff.big
  littleGroup.server[litIndex] = diff.big
  bigGroup.server[bigIndex] = diff.lit
  littleGroup.gc = _.sum(littleGroup.capacity) - _.max(littleGroup.capacity)
  bigGroup.gc = _.sum(bigGroup.capacity) - _.max(bigGroup.capacity)
  diff.lit.group = bigGroup
  diff.big.group = littleGroup
  true


diff = true
while diff
  groups.sort (a, b)-> a.gc - b.gc
  diff = switchGroup groups[0]

#
# Génération des fichiers
#


server.sort (a, b)-> a.i - b.i

print =
  for ser in server
    if ser.group then "#{ser.line.r} #{ser.s} #{ser.group.p}"
    else 'x'
fs.writeFileSync('out', print.join('\n'))

server.sort (a, b)-> a.c - b.c
fs.writeFileSync('server', server.map((ser)-> "#{ser.z} #{ser.c} #{ser.group?.p}").join('\n'))
fs.writeFileSync('group', groups.map((g)-> g.gc + " " + g.c).join('\n'))

print =
  for r in [0..R - 1]
    grid[r].join('')
printAvailable = _.clone print
for r in [0..R - 1]
  printAvailable[r] += '\t' + lines[r].capacity + '\t' + lines[r].section.map((sec)-> sec.available).join(',')

fs.writeFileSync('grid2', printAvailable.join('\n'))

byEfficiency.sort (a, b)-> (-(a.c / a.z) + (b.c / b.z)) * 100 - a.z + b.z
fs.writeFileSync('byEfficiency', byEfficiency.map((ser)-> "#{ser.z} #{ser.c}").join('\n'))

groups.sort (a, b)-> a.gc - b.gc
console.log groups[0].gc
#  if score < groups[0].gc
#    score = groups[0].gc
#    powww_ = powww
#    console.log score, powww_
#
#  console.log powww
#console.log score, powww_
